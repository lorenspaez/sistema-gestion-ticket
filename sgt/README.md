###Integrantes Grupo10:###
		    
		    **Juan Caniguante**
		    **Simon Rivera**
		    **Lorens Paez**
        

**Stack Utilizado**:
		-PostgreSql
                -Express
                -Angular-cli
                -Nodejs
                -Sequelize-cli

                
Dependencias Instaladas: **body-parser, cors, express, morgan, pg, pg-hstore, sequelize, nodemon**
Tambien es necesario tener **postgres** instalado con la siguiente Base de dato:

``-username:"postgres"``

``-password:"123"``

``-database:"postgres"``

Para poder instalar estas se deben utilizar los siguientes **comandos**:

-En el directorio "Frontend":&nbsp;

``npm install -g @angular/cli@7.0.7``

                            
-Aplicar dentro del directorio "Server":  

`npm install`  
                  `npm install -g sequelize-cli`
                                   `npm install --save sequelize pg pg-hstore`&nbsp;  
`sequelize db:migrate`

Para la utilización de Login y registro de estudiantes es necesario tener creado un administrador manualmente, utilizando postman o alguna otra herramienta.

Se debe ingresar los siguientes parametros en el body de la url **localhost:8000/api/admin**:

         -nombre (STRING)
         -email (STRING)
         -tipo (BOOLEAN)
         -pass (STRING)

La generación de los tickets esta implementada de a nivel de base de datos, con sus respectivas restricciones como por ejemplo que estos no puedes ser creados si no existen estudiantes.


Esta puede ser realizada a tráves de la herramienta postman por el momento utilizando el siguiente body:

    -verificar(BOOLEAN)
    -Idest




