import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { QRCodeModule } from 'angularx-qrcode';
import {RouterModule, Routes} from '@angular/router'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule} from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { HomeComponent } from './componentes/home/home.component';
import { RegistroComponent } from './componentes/registro/registro.component';
import { NavbarComponent } from './componentes/navbar/navbar.component';
import { LoginComponent } from './componentes/login/login.component';
import {AuthService} from './servicios/auth.service';
import{ValidateService} from './servicios/validate.service';
import{TicketService} from './servicios/ticket.service';
import { PreguntasComponent } from './componentes/preguntas/preguntas.component';
import { QrComponent} from './componentes/QR/qr.component';
import { HistorialComponent } from './componentes/historial/historial.component';
import { ValidarTicketComponent } from './componentes/validar-ticket/validar-ticket.component';

const appRoutes: Routes =  [
  {path: '', component: HomeComponent},
  {path: 'registro', component: RegistroComponent},
  {path: 'login', component: LoginComponent},
  {path: 'preguntas', component: PreguntasComponent},
  {path: 'qr', component: QrComponent},
  {path: 'historial', component: HistorialComponent},
  {path: 'validar', component: ValidarTicketComponent}

]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegistroComponent,
    NavbarComponent,
    LoginComponent,
    PreguntasComponent,
    QrComponent,
    HistorialComponent,
    ValidarTicketComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    QRCodeModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  
  providers: [ValidateService, AuthService, TicketService],
  bootstrap: [AppComponent]
})
export class AppModule { }
