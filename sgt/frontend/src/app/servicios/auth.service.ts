import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { map } from 'rxjs/operators';
import {tokenNotExpired} from 'angular2-jwt';
import {Ticket} from '../modelos/ticket';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authToken: any;
  estudiante: any;
  administrador: any;
  ticket: any;
  token: boolean=false;
  token2: boolean= false;


  constructor(private http: Http) { }

  registerUser(estudiante){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:8000/api/auth/signup', estudiante, {headers: headers})
      .pipe(map(res => res.json()));
  }

  authenticateUser(administrador){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:8000/api/auth/login', administrador, {headers: headers})
      .pipe(map(res => res.json()));
  }

  storeUserData(token, administrador){
    localStorage.setItem('id_token', token);
    localStorage.setItem('administrador', JSON.stringify(administrador));
    this.authToken = token;
    this.administrador = administrador;

  }

  generateTicket(ticket){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:8000/api/ticket', ticket, {headers: headers})
      .pipe(map(res => res.json()));
  }

  getOneTicket(num: number){
    return this.http.get('http://localhost:8000/api/ticket'+ `/${num}` , this.ticket)
  }

  updateTicket(ticket){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put('http://localhost:8000/api/ticket'+ `/${ticket.id}` , ticket, {headers: headers})
      .pipe(map(res => res.json()));
  }
  
  getView(){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:4200/registro', {headers: headers})
      .pipe(map(res => res.json()));
  }

  logout(){
    this.authToken = null;
    this.administrador = null;
    localStorage.clear();
  }

  loadToken(){
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }


}
