import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidateService {

  constructor() { }

  validateRegistro(estudiante){
    if(estudiante.nombre == undefined || estudiante.email == undefined || estudiante.rol == undefined || estudiante.huella == undefined || estudiante.Idadmin == undefined){
      return false;
    }else{
      return true;
    }
  }

  validateRegistroTicket(ticket){
    if(ticket.Idest == undefined){
      return false;
    }else{
      return true;
    }
  }

  validacionTicket(ticket){
    if(ticket.id == undefined){
      return false;
    }else{
      return true;
    }
  }

  validateEmail(email){
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

}
