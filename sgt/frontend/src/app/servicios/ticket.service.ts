import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {Ticket} from '../modelos/ticket';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  selectedTicket: Ticket;
  tickets: Ticket[];
  ticket: any;
  constructor(private http: HttpClient) { 
    this.selectedTicket = new Ticket();
  }

  getTicket(){
    return this.http.get('http://localhost:8000/api/ticket');
  }

  postTicket(Ticket: Ticket){
    return this.http.post('http://localhost:8000/api/ticket', Ticket);
  }
}