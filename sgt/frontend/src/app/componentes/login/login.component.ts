import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../servicios/auth.service';
import {Router} from '@angular/router';

declare var M: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string;
  pass: string;
  tipo: boolean;

  constructor(
    private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
  }

  onLoginSubmit(){
    const administrador ={
      email: this.email,
      pass: this.pass,
      tipo: this.tipo
    }
    this.authService.authenticateUser(administrador).subscribe(data =>{
      if(!data.success){
        M.toast({html: 'Ingreso Correcto'});
        if(administrador.tipo == true){
          this.router.navigate(['validar']);
          this.authService.token2 = true;
          this.authService.token = false;
        }else{
          this.router.navigate(['registro']);
          this.authService.token = true;
          this.authService.token2= false;
        }
      }else{
        //this.authService.storeUserData(data.token, data.administrador);
        M.toast({html: 'Datos incorrectos'});
        this.router.navigate(['login']);
      }
    });
  }

}
