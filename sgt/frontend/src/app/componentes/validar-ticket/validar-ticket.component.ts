import { Component, OnInit } from '@angular/core';
import {ValidateService} from '../../servicios/validate.service';
import {AuthService} from '../../servicios/auth.service';
import { TicketService} from '../../servicios/ticket.service';
import { Ticket} from '../../modelos/ticket'

declare var M: any;

@Component({
  selector: 'app-validar-ticket',
  templateUrl: './validar-ticket.component.html',
  styleUrls: ['./validar-ticket.component.css']
})
export class ValidarTicketComponent implements OnInit {
  ticket1 : any;
  id: number;
  verificar: boolean;
  Idest: string;
  qr: string;

  constructor(private validateService: ValidateService,
    private authService: AuthService,
    private ticketService: TicketService) { }

  ngOnInit() {
  }

  onRegisterSubmit(){
    const ticket = {
      id: this.id,
      qr: this.qr,
      verificar: this.verificar,
      Idest: this.Idest
    }


    if(!this.validateService.validacionTicket(ticket)){
      M.toast({html: 'Completar todos los campos'});
      return false;
    }else{ 
      ticket.verificar = true;
      this.authService.updateTicket(ticket).subscribe(data => {
        if(data.success){
          M.toast({html: 'Falla en validación'});
        }else{
          M.toast({html: 'Ticket validado! Estudiante puede retirar colación'});
        }
      });
      //this.authService.getOneTicket(ticket.id);
    } 

  }
}
