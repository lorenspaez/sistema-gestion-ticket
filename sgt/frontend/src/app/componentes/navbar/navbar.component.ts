import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../servicios/auth.service';
import {Router} from '@angular/router';

declare var M: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  loggedIn(){
    return this.authService.token;
  }

  loggedIn2(){
    return this.authService.token2;
  }

  onLogoutClick(){
    this.authService.logout();
    M.toast({html: 'Salida correcta'});
    this.router.navigate(['/login']);
    this.authService.token = false;
    this.authService.token2 = false;
    return false;
  }
}
