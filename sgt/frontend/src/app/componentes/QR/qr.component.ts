import { Component, OnInit } from '@angular/core';
import {ValidateService} from '../../servicios/validate.service';
import {AuthService} from '../../servicios/auth.service';
import { TicketService} from '../../servicios/ticket.service';
import { Ticket} from '../../modelos/ticket'

export class NgxQrCode {
  text: string;
}

declare var M: any;

@Component({
  selector: 'app-root',
  templateUrl: './qr.component.html',
  styleUrls: ['./qr.component.css'],
  providers: [TicketService]
})
export class QrComponent implements OnInit{
  id: number;
  verificar: boolean;
  Idest: string;
  qr: string;

  constructor(
    private validateService: ValidateService,
    private authService: AuthService,
    private ticketService: TicketService
  ) {
    console.log('AppComponent running');
    this.qr = ' ';
  }

  changeValue(newValue: string): void {
    this.qr = newValue;
  }

  ngOnInit() {
    this.getTicket();
  }

  getTicket(){
    this.ticketService.getTicket()
    .subscribe(res => {
      this.ticketService.tickets = res as Ticket[];
    });
  }

  onRegisterSubmit(){
    const ticket = {
      id: this.id,
      qr: this.qr,
      verificar: this.verificar,
      Idest: this.Idest
    }


    if(!this.validateService.validateRegistroTicket(ticket)){
      M.toast({html: 'Completar todos los campos'});
      return false;
    }
    this.authService.generateTicket(ticket).subscribe(data => {
      if(data.success){
        M.toast({html: 'Registro fallido'});
      }else{
        var num = (this.ticketService.tickets[this.ticketService.tickets.length-1].id + 1);
        ticket.qr = `${num}` + '-' + ticket.Idest;
        ticket.id = num;
        this.authService.updateTicket(ticket).subscribe(data => {
          if(data.success){
            M.toast({html: 'Registro fallido'});
          }else{
            M.toast({html: 'Ticket generado!'});
          }
        
        });
        }
    });
  }
}
