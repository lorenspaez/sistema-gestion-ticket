import { TestBed, async } from '@angular/core/testing';
import { QrComponent } from './qr.component';

describe('QrComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        QrComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(QrComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'angularx-qrcode-sample-app'`, () => {
    const fixture = TestBed.createComponent(QrComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('angularx-qrcode-sample-app');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(QrComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to angularx-qrcode-sample-app!');
  });
});
