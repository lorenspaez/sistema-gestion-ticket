import { Component, OnInit } from '@angular/core';
import { TicketService} from '../../servicios/ticket.service';
import { Ticket} from '../../modelos/ticket'
import {AuthService} from '../../servicios/auth.service';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css'],
  providers: [TicketService]
})
export class HistorialComponent implements OnInit {

  constructor(private ticketService: TicketService, private authService: AuthService) {}
  
  ngOnInit() {
    this.getTickets();
  }


  getTickets(){
    this.ticketService.getTicket()
      .subscribe(res => {
        this.ticketService.tickets = res as Ticket[];
      });
      var currentTime = new Date();
      console.log(currentTime.getDay);
  }
}
