import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ValidateService} from '../../servicios/validate.service';
import {AuthService} from '../../servicios/auth.service';


declare var M: any;

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  nombre: String;
  email: String;
  rol: String;
  huella: String;
  Idadmin: String;

  constructor(
    private validateService: ValidateService,
    private authService: AuthService,
    private  router: Router
    ) { }

  ngOnInit() {
  }

  onRegisterSubmit(){
    const estudiante = {
      rol: this.rol,
      nombre: this.nombre,
      email: this.email,
      huella: this.huella,
      Idadmin: this.Idadmin
    }

    //required fields
    if(!this.validateService.validateRegistro(estudiante)){
      M.toast({html: 'Completar todos los campos'});
      return false;
    }

    //validate email
    if(!this.validateService.validateEmail(estudiante.email)){
      M.toast({html: 'Email inválido'});
      return false;
    }

    //register user
    this.authService.registerUser(estudiante).subscribe(data => {
      if(data.success){
        M.toast({html: 'Registro fallido'});
        this.router.navigate(['/registro']);
      }else{
        M.toast({html: 'Estudiante Registrado!'});
        this.router.navigate(['/registro']);
      }
    })
  }

}
