export class Administrador {
    
    constructor(nombre = '', email = '', pass = '', tipo = false){
        this.nombre = nombre;
        this.email = email;
        this.pass = pass;
        this.tipo = tipo;

    }
    
    nombre: string;
    email: string;
    pass: string;
    tipo: boolean;
}