export class Ticket {
    
    constructor(id= 0, qr = '',  verificar = false, Idest = ''){
        this.id= id;
        this.qr = qr;
        this.verificar = verificar;
        this.Idest = Idest;

    }
    id: number;
    qr: string;
    verificar: boolean;
    Idest: string
}
