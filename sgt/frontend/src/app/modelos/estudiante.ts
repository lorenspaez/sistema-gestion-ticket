export class Estudiante {
    
    constructor(nombre ='', email = '', rol = '', huella ='', Idadmin = ''){
        this.email = email;
        this.rol = rol;
        this.nombre = nombre;
        this.huella = huella;
        this.Idadmin = Idadmin;
    }
    
    nombre: string;
    email: string;
    rol: string;
    huella: string;
    Idadmin: string;
}
