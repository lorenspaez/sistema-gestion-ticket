const adminController = require('../controllers').admin;
const ticketController = require('../controllers').ticket;
const estudianteController = require('../controllers').estudiante;

module.exports = (app) => {
  app.get('/api', (req, res) => res.status(200).send({
    message: 'Welcome to the Todos API!',
  }));


//administrador
app.post('/api/admin', adminController.create);
app.post('/api/auth/login', adminController.login);
app.get('/api/admin',adminController.list);
//app.get('/api/admin/:email',adminController.retrieveByEmail)
//estudiante
app.post('/api/auth/signup', estudianteController.create);
app.get('/api/auth/signup', estudianteController.list);
//app.get('/api/estudiante/:id',estudianteController.retrieveById);
app.get('/api/estudiante/rol/:rol',estudianteController.retrieveByRol);
//Ticket
app.post('/api/ticket',ticketController.create);
app.get('/api/ticket',ticketController.list);
app.get('/api/ticket/:id',ticketController.retrieveByID);
app.put('/api/ticket/:id',ticketController.update);
  
  
 
 
  //app.get('/api/admin', adminController.list);
};

