const Admin = require('../models').Admin;
const Estudiante = require('../models').Estudiante;

module.exports = {
  create(req, res) {
    return Admin
      .create({
        nombre: req.body.nombre,
        email: req.body.email,
        tipo: req.body.tipo,
        pass: req.body.pass,
      })
      .then(admin => res.status(201).send(admin))
      .catch(error => res.status(400).send(error));
  },
  list(req, res){
      return Admin
      .findAll({
          include:[{
              model: Estudiante,
              as: 'estudiante',
          }]
      })
    .then(admin => res.status(200).send(admin))
    .catch(error => res.status(400).send(error));
  },
  retrieveByEmail(req,res){
    return Admin
    .findByPk(req.body.email,{
        include:[{
          model:Estudiante,
          as:"estudiante"
        }],
    })
    .then(admin =>{
      if(!admin){
        return res.status(404).send({
          message: 'Administrador Not Found',
        });
      }
      return res.status(200).send(admin);
    })
    .catch(error => res.status(400).send(error));
  },
  login(req,res){
    console.log(req.body.email);
    console.log(req.body.pass);
    Admin.find({ where: {email: req.body.email}}).then(admin =>{
      if(admin != null){
        if(admin.pass == req.body.pass){
          return res.status(200).send({
            message: 'OK'});
        }
        else{
          return res.status(404).send({
            message: 'Clave incorrecta' 
          });
        }
      }
      else{
        return res.status(404).send({message: 'Email no encontrado'});
      }
    })
    .catch(error => res.status(400).send(error))
    /*if(validAdmin(req.body)){
      return res.json({
        message:'admin valido'
      });
    }
    
    return res.status(400).send({
      message: 'admin invalido'
    });*/
  }
};
function validAdmin(admin){
  return 
};
/*function validAdmin(admin) {
  const validEmail = (typeof admin.email == 'string') && (admin.email.trim() != '');
  console.log(admin);
  return validEmail; 
};*/