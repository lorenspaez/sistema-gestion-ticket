const Estudiante = require('../models').Estudiante;
const Ticket = require('../models').Ticket;
module.exports = {
  create(req, res) {
    return Estudiante
      .create({
        nombre: req.body.nombre,
        email: req.body.email,
        rol: req.body.rol,
        huella:req.body.huella,
        Idadmin:req.body.Idadmin,
      })
      .then(estudiante => res.status(201).send(estudiante))
      .catch(error => res.status(400).send(error));
  },
  list(req, res){
      return Estudiante
      .findAll({
          include:[{
              model: Ticket,
              as: 'ticket',
          }]
      })
    .then(estudiante => res.status(200).send(estudiante))
    .catch(error => res.status(400).send(error));
  },
   retrieveByRol(req,res){
      console.log(req.params.rol);
      return Estudiante
      .findById(req.params.rol, {
        include:[{
          model: Ticket,
          as: 'ticket',
         }]
      })
      .then(estudiante =>{
        if(!estudiante){
          return res.status(404).send({
            message: 'Estudiante Not Found',
          });
        }
        console.log(estudiante);
        return res.status(200).send(estudiante);
      })
      .catch(error => res.status(400).send(error));
  },
  
  /*retrieveByRol(req,res){
      return Estudiante
      .findAll({
          where:{rol: req.params.rol}
     /* }, {
        include:[{
          model: Ticket,
          as: 'ticket',
        }]*/
    /*  })
      .then(estudiante => {
        if(!estudiante){
            return res.status(404).send({
              message: 'Estudiante Not Found',
            });
        }
        return res.status(200).send(estudiante);
      })
  },*/

};
