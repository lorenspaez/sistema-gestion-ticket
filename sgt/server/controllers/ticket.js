const Ticket = require('../models').Ticket;

module.exports = {
  create(req, res) {
    return Ticket
      .create({
        //imagen: req.body.imagen,
        verificar:req.body.verificar,
        qr:req.body.qr,
        Idest: req.body.Idest,
      })
      .then(ticket => res.status(201).send(ticket))
      .catch(error => res.status(400).send(error));
  },
  list(req, res){
    return Ticket
    .findAll()
    .then(ticket => res.status(200).send(ticket))
    .catch(error => res.status(400).send(error));
},
  retrieveByID(req,res){
    return Ticket
    .findById(req.params.id)
    .then(ticket =>{
      if(!ticket){
        return res.status(404).send({
          message: "Ticket no encontrado",
        });
      }
      return res.status(200).send(ticket);
    })
    .catch(error => res.status(400).send(error));
  },
  update(req, res) {
    return Ticket
      .findById(req.body.id)
      .then(ticket => {
        if (!ticket) {
          return res.status(404).send({
            message: 'Ticket Not Found',
          });
        }
        return ticket
          .update({
            qr: req.body.qr || ticket.qr,
            verificar: req.body.verificar || ticket.verificar
          })
          .then(() => res.status(200).send(ticket))  // Send back the updated todo.
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  }
};