const admin = require('./admin');
const ticket = require('./ticket');
const estudiante = require('./estudiante');

module.exports = {
  admin,
  ticket,
  estudiante,
};