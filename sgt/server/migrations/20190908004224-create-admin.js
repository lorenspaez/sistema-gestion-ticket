module.exports = {
  up: (queryInterface, Sequelize) => 
    queryInterface.createTable('Admins', {
      /*id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },*/
      nombre: {
        allowNull: false,
        type: Sequelize.STRING
      },
      email: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      tipo: {
        allowNull: false,
        type: Sequelize.BOOLEAN
      },
      pass: {
        allowNull: false,
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }),
  down: (queryInterface) => 
    queryInterface.dropTable('Admins'), 
};