module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('Estudiantes', {
      /*id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },*/
      nombre: {
        allowNull: false,
        type: Sequelize.STRING
      },
      email: {
        allowNull: false,
        type: Sequelize.STRING
      },
      rol: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      huella: {
        allowNull: false,
        type: Sequelize.STRING
      },
      Idadmin: {
        type: Sequelize.STRING,
        //allowNull: false,
        onDelete: 'CASCADE',
        references:{
          model: 'Admins',
          key: 'email'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }),
  down: (queryInterface) => 
    queryInterface.dropTable('Estudiantes'),
};