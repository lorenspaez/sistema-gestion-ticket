module.exports = {
  up: (queryInterface, Sequelize) => 
    queryInterface.createTable('Tickets', 
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      verificar: {
        allowNull: false,
        type: Sequelize.BOOLEAN
      },
      qr: {
        allowNull: false,
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      Idest:{
        type:Sequelize.STRING,
        allowNull: false,
        onDelete: 'CASCADE',
        references:{
          model: 'Estudiantes',
          key:'rol',
        }
      }
    }),

  down: (queryInterface) => 
    queryInterface.dropTable('Tickets'),
};