module.exports = (sequelize, DataTypes) => {
  const Estudiante = sequelize.define('Estudiante', {
    nombre:{
      type: DataTypes.STRING,
      allowNull: false,
    } ,
    email:{
      type: DataTypes.STRING,
      allowNull: false,
    },
    rol:{
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
      unique: true,
    },
    huella:{
      type: DataTypes.STRING,
      allowNull: false,
    },
  });
  Estudiante.associate = (models) => {
    // associations can be defined here
    Estudiante.belongsTo(models.Admin,{
      foreignKey: 'Idadmin',
      onDelete: 'CASCADE',
    });

    Estudiante.hasMany(models.Ticket, {
      foreignKey: 'Idest',
      as: 'ticket',
    });
  };

  return Estudiante;
};