module.exports = (sequelize, DataTypes) => {
  const Admin = sequelize.define('Admin', {
    nombre:{
      type: DataTypes.STRING,
      allowNull: false,
    }, 
    email:{
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,        
      //unique: true, 
    },
    tipo:{
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    pass:{
      type: DataTypes.STRING,
      allowNull: false,
    }  
  });
  Admin.associate = (models)=> {
    // associations can be defined here
    
    Admin.hasMany(models.Estudiante, {
      foreignKey: 'Idadmin',
      as: 'estudiante ',
    });
  };
  return Admin;
  
};