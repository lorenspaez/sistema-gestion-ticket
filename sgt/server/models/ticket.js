module.exports = (sequelize, DataTypes) => {
  const Ticket = sequelize.define('Ticket', {
    qr:{
      type: DataTypes.STRING,
      allowNull: false,
    },
    verificar:{
      type: DataTypes.BOOLEAN,
      allowNull: false,
    }
  });
  Ticket.associate = function(models) {
    // associations can be defined here
    Ticket.belongsTo(models.Estudiante, {
      foreignKey: 'Idest',
      onDelete: 'CASCADE',
    });
  };
  return Ticket;
};